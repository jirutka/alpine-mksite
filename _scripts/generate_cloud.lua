#!/usr/bin/lua

local math = require("math")
local lyaml = require("lyaml")

-- version_to_integer converts an Alpine version to an integer that, while
-- generally meaningless, will be strictly greater than a lower numbered
-- version. Handles 3.10.2 as well as 3.10.0_rc5 formats.
--
-- Note: If there are more than 3 sets of integers in the version number will
-- return incorrect results. (ex: 3.10.2.3_rc3)
local function version_to_integer(v)
    if v == "edge" then
        return 0 -- edge should always sort last
    end

    local i = 10000
    local out = 0

    for p in v:gmatch("[0-9]+") do
        out = out + (tonumber(p) * i)
        i = i / 10
    end

    return math.floor(out)
end

t = { clouds={ aws={} } }

for i = 1, #arg do
    local f = assert(io.open(arg[i]))
    local temp = {}

    for version, profile_build in pairs(lyaml.load(f:read("*a"))) do
        for _, images in pairs(profile_build) do
            for _, v in pairs(images) do
                local cy,cm,cd = v.creation_date:match("(%d+)-(%d+)-(%d+)")
                local ey,em,ed = v.end_of_life:match("(%d+)-(%d+)-(%d+)")
                if version ~= "edge" then
                    if temp[version] == nil then
                        temp[version] = {}
                    end

                    artifacts = {}

                    for k, v in pairs(v.artifacts) do
                        table.insert(artifacts, { region=k, image_id=v })
                    end

                    table.sort(artifacts, function(a, b) return a.region < b.region end)

                    table.insert(temp[version], {
                        release = v.release,
                        arch = v.arch,
                        version = version,
                        creation_date = os.date("%b %d, %Y", os.time{year=cy, month=cm, day=cd}),
                        end_of_life = os.date("%b %d, %Y", os.time{year=ey, month=em, day=ed}),
                        artifacts = artifacts,
                    })
                end
            end
        end
    end

    for k, v in pairs(temp) do
        table.insert(t.clouds.aws, { version=k, releases=v })
    end

    table.sort(t.clouds.aws, function(a, b)
        return version_to_integer(a.version) > version_to_integer(b.version)
    end)
end

io.write(lyaml.dump{t})
