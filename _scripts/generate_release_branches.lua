#!/usr/bin/lua

lyaml = require("lyaml")

t = {}
for i = 1,#arg do
	local f = assert(io.open(arg[i]))

	for _,v in pairs(lyaml.load(f:read("*a"))) do
		if v.eol_date ~= nil then
			local y,m,d = v.eol_date:match("(%d+)-(%d+)-(%d+)")
			v.eol_datestr = os.date("%B, %Y", os.time{year=y, month=m, day=d})
		end
		if v.releases then
			local rels = {}
			for _, r in pairs(v.releases) do
				r.url = string.gsub(r.notes or "","(.*).md$", "../%1.html")
				table.insert(rels, r)
			end
			v.releases = rels
			v.latest = v.releases[1]
		end

		table.insert(t, v)
	end
	f:close()
end

io.write(lyaml.dump{t})
