---
title: Alpine release branches
---

# Release Branches

There are several release branches for Alpine Linux available at the same
time. Each May and November we make a release branch from edge. The _main_
repository is typically supported for 2 years and the _community_ repository
is supported until next stable release.

Security fixes beyond that can be made on request when there are patches available.

<div class="releases">
<table class="pure-table pure-table-horizontal">
 <thead>
  <tr>
  <th nowrap>Branch</th>
  <th nowrap>Branch date</th>
  <th nowrap>Git Branch</th>
  <th nowrap>Minor releases</th>
  <th nowrap>End of support</th>
  </tr>
 </thead>
 <tbody>
 {{#releases/release-branches}}
 <tr>
  <td><a href="https://dl-cdn.alpinelinux.org/alpine/{{rel_branch}}/">{{rel_branch}}</a></td>
  <td nowrap>{{branch_date}}</td>
  <td nowrap><a href="https://gitlab.alpinelinux.org/alpine/aports/-/commits/{{git_branch}}">{{git_branch}}</a></td>
  <td>
   {{#releases}}
   |&nbsp;<a href="{{url}}">{{version}}</a>
   {{/releases}}
  </td>
  <td nowrap><time>{{eol_date}}</time></td>
 </tr>
 {{/releases/release-branches}}
 </tbody>
</table> 
</div>
