---
title: 'Alpine 3.15.0 released'
date: 2021-11-24
---

Alpine Linux 3.15.0 Released
============================

We are pleased to announce the release of Alpine Linux 3.15.0, the first in
the v3.15 stable series.

<a name="highlights">Highlights</a>
----------

* Linux kernels [5.15](https://kernelnewbies.org/Linux_5.15) (LTS)
* llvm [12](https://releases.llvm.org/12.0.0/docs/ReleaseNotes.html)
* nodejs [16.13](https://nodejs.org/en/blog/release/v16.13.0/) (LTS) / nodejs-current [17.0](https://nodejs.org/en/blog/release/v17.0.0/)
* postgresql [14](https://www.postgresql.org/about/news/postgresql-14-released-2318/)
* openldap [2.6](https://www.openldap.org/software/release/announce.html)
* ruby [3.0](https://www.ruby-lang.org/en/news/2020/12/25/ruby-3-0-0-released/)
* rust [1.56](https://blog.rust-lang.org/2021/10/21/Rust-1.56.0.html)
* openjdk [17](https://openjdk.java.net/projects/jdk/17/)
* kea [2.0](https://www.isc.org/blogs/2021-09-29-kea-2.0/)
* xorg-server [21.1](https://lists.x.org/archives/xorg/2021-October/060799.html)
* GNOME [41](https://help.gnome.org/misc/release-notes/41.0/)
* KDE Plasma 5.23 / KDE Applications 21.08 / Plasma Mobile Gear 21.10
* Support for disk encryption in installer
* Support for out-of-tree kernel modules via [AKMS](https://github.com/jirutka/akms)
  (inspired by DKMS)
* Initial support for UEFI [Secure Boot][secure-boot] on x86_64

<a name="significant_changes">Significant changes</a>
-------------------

* Kernel modules are now compressed with gzip.
* Framebuffer drivers have been disabled in kernel and replaced by simpledrm.
* `qt5-qtwebkit` and related packages have been removed due to lack of upstream
  support.
* The MIPS64 port is discontinued. The architecture is EOL and no new releases will be made. See [TSC#27][tsc-27].

<a name="upgrade_notes">Upgrade notes</a>
-------------

* As always, make sure to use `apk upgrade --available` when switching between
  major versions.
* New 4096 bit RSA keys are now used for package signatures. Make sure
  alpine-keys>=2.4.0-r0 is installed on your system before upgrading.
* Multiple major versions of PostreSQL can now be installed in parallel to facilitate upgrades.
  See the PostgreSQL [upgrade notes][psql-upgrade] for more details.
* radvd no longer enables forwarding for IPv6. If still required, make sure
  `net.ipv6.conf.all.forwarding` is enabled.
* Several ruby packages have been moved to dedicated aports, and others have
  merged into a single package. See [the wiki][ruby-302] for details.
  
<a name="deprecation_notes">Deprecation notes</a>
-----------------

* `sudo` will be moved to community in Alpine Linux 3.16. The suggested
  replacement is `doas`, which is available in main. See [TSC#1][tsc-1]
* `php7` is being phased out, as the last release, 7.4 will have only 1 year of
  security support left.

<a name="changes">Changes</a>
-------

The full list of changes can be found in the [wiki][8],  [git log][9] and [bug tracker][10].

<a name="credits">Credits</a>
-------

Thanks to everyone sending in patches, bug reports, new and updated aports,
and to everyone helping with writing documentation, maintaining the
infrastructure, or has contributed in any other way!

Thanks to [GIGABYTE][1], [Linode][2], [Fastly][3], [IBM][4], [Equinix Metal][5],
[vpsFree][6] and [RapidSwitch][7] for providing us with hardware and
hosting.

[1]: https://www.gigabyte.com/
[2]: https://linode.com
[3]: https://www.fastly.com/
[4]: https://ibm.com/
[5]: https://www.equinix.com/
[6]: https://vpsfree.org
[7]: https://www.rapidswitch.com/
[8]: https://wiki.alpinelinux.org/wiki/Release_Notes_for_Alpine_3.15.0
[9]: https://git.alpinelinux.org/cgit/aports/log/?h=v3.15.0
[10]: https://gitlab.alpinelinux.org/alpine/aports/issues?scope=all&utf8=%E2%9C%93&state=closed&milestone_title=3.15.0
[psql-upgrade]:https://wiki.alpinelinux.org/wiki/Release_Notes_for_Alpine_3.15.0#PostgreSQL_multiple_major_versions
[secure-boot]:https://wiki.alpinelinux.org/wiki/UEFI_Secure_Boot
[tsc-1]:https://gitlab.alpinelinux.org/alpine/tsc/-/issues/1
[tsc-27]:https://gitlab.alpinelinux.org/alpine/tsc/-/issues/27
[ruby-302]:https://wiki.alpinelinux.org/wiki/Release_Notes_for_Alpine_3.15.0#Ruby_3.0.2

### aports Commit Contributors

<pre>
6543
AN3223
Adam Jensen
Adam Plumb
Aiden Grossman
Akihiro Suda
Alain Greppin
Alan Diwix
Aleks Bunin
Alex Denes
Alex McGrath
Alex Sivchev
Alex Xu (Hello71)
Alexandre Jousset
Alexey Minnekhanov
Alexey Yerin
Alistair Francis
Anatoli Babenia
Andrei Jiroh Eugenio Halili
André Klitzing
Andy Hawkins
Andy Postnikov
Anees
Anjandev Momi
Antoni Aloy Torrens
Apo Apangona
Ariadne Conill
Arnav Dugar
Arnavion
Bart Ribbers
Bobby The Builder
Bradford D. Boyle
Brett Carlock
Carlo Dé
Carlo Landmeter
Christoph Weiss
Clayton Craft
Cormac Stephenson
Craig Andrews
Craig Comstock
Curt Tilmes
Cássio Ribeiro Alves de Ávila
Damian Kurek
Danct12
Daniel Néri
Dave Henderson
Dave Tucker
David A. Hannasch
David Demelier
David Florness
Dekedro
Dennis Günnewig
Dennis Krupenik
Dennis Przytarski
Dermot Bradley
Devin Lin
Diaz Urbaneja Victor Diego Alenjandro
Dmitry Kruchko
Dominique Martinet
Drew DeVault
Duncan Bellamy
Dylan Van Assche
Emanuele Sorce
Erik Larsson
Fabio Huser
Felix
Fernando Casas Schossow
FollieHiyuki
Francesco Camuffo
Francesco Colista
GP Orcullo
Galen Abell
Gennady Feldman
Geod24
George Hopkins
Glenn Strauss
GreyXor
Guillaume Quintard
Haelwenn (lanodan) Monnier
Hani Shawa
Henrik Riomar
Holger Jaekel
Hossein Hosni
Hugo Rodrigues
Ian Bashford
Ihor Antonov
InsanePrawn
Iskren Chernev
Ivan Maidanski
J0WI
Jacob Panek
Jake Buchholz
Jake Buchholz Göktürk
Jakob Hauser
Jakub Jirutka
Jakub Panek
Jami Kettunen
Jan Tatje
Jared Allard
Jesse Chan
Joel Selvaraj
Johannes Heimansberg
Johannes Marbach
John Longe
Jonas
Jonas Heinrich
Jordan Christiansen
Josef Vybíhal
José Alberto Orejuela García
Julian Weigt
JuniorJPDJ
Justin Berthault
Kaarle Ritvanen
Kevin Daudt
Kevin Thomas
KikooDX
Konstantin Kulikov
Krystian Chachuła
Kye W. Shi
Laurent Bercot
Leo
Leon Marz
Leonardo Arena
Linux User
Lonjil
Lonnie
Luca Weiss
Lucas Ramage
Lucidiot
Maarten van Gompel
Magnus Sandin
Marian Buschsieweke
Mark Pashmfouroush
MarlinMr
Martijn Braam
Martin Kaesberger
Martin Vahlensieck
Marvin Preuss
Matthew T Hoare
Maxim Karasev
Mengyang Li
Michael
Michael Ekstrand
Michael Pirogov
Michael Truog
Michał Adamski
Michał Polański
Mickaël Schoentgen
Mike Crute
Milan P. Stanić
Miles Alan
Minecrell
Misthios
Natanael Copa
Neil O'Fix
Nevadim 'ov
Newbyte
Nicholas Hanley
Nick Chiu
Nick Gaya
Nico Schottelius
Nikita Travkin
Noel Kuntze
Nulo
Oleg Titov
Oliver Smith
Olliver Schinagl
Ondřej Ešler
Otto Modinos
Pablo Correa Gómez
Patrick Allaert
Patrick Gansterer
Paul Bredbury
Paul Spooren
Pedro Filipe
Pedro Lucas Porcellis
Pellegrino Prevete
Peter van Dijk
Pietro
Piraty
R4SAS
Raatty
Ralf Rachinger
Rejah Rehim
Risgit
River Dillon
Robert Günzler
Rodrigo Lourenço
Roshless
Ryan Barnett
Saijin-Naib
Sam Bowlby
Sean
Sean McAvoy
Sheila Aman
Shen
Simon Frankenberger
Simon Rupf
Simon Ser
Simon Zeni
Sodface
Sol Fisher Romanoff
Stacy Harper
Stefan Reiff
Steven Vanden Branden
Sughosha
Sylvain Prat
Sören Tempel
Terra
Thomas Grainger
Thomas Kienlen
Thomas Liske
Timo Teräs
Timothy Legge
Tom Lebreux
Tom Tsagk
Vladislav Sharapov
Will Sinatra
Wolf
Yonggang Luo
Youfu Zhang
Zach DeCook
andrea rota
andrewmiskell
boomanaiden154
dekzi
djv
donoban
ed neville
fijam
fkf9
guddaff
j.r
jvoisin
kasperk81
kedap
knuxify
kpcyrd
krystianch
ktprograms
lafleur
linear
linear cannon
macmpi
messense
mio
nibon7
nick black
omni
opal hart
otoir9ouzeengair@protonmail.com
pcworld
pexcn
pglaum
prspkt
psykose
ptrcnull
samuel norbury
shhhum
techknowlogick
tiotags
tronfortytwo
ur4t
voroskoi
wener
Éloi Rivard
Érico Nogueira
Óliver García
</pre>
